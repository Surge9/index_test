module BinaryTreeSearch

  # Represents leaf node for the binary search tree
  class EmptyNode

    def to_a
      []
    end

    def include?(*)
      false
    end

    def entries_for(word)
      {}
    end

    def push(*)
      false
    end

    def inspect
      "#"
    end

    def serialize(file)
    	file << "-1\n"
    end

  end
end
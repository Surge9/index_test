require './node.rb'
include BinaryTreeSearch

module BinaryTreeSearch
  # Provides indexing and index search using binary tree
  class IndexTree
    # dictionary for file names
    attr_accessor :dict
    # search tree root
    attr_accessor :root
    # index file name
    attr_accessor :index_filename

    # default index file name
    INDEX_FILE_NAME = 'id.x'

    def initialize(idx_filename=nil)
      @dict = []
      @root = nil
      @index_filename = idx_filename || INDEX_FILE_NAME
    end

    # push/create value in the search tree
    def push(v)
      @root.nil? ? @root = Node.new(v) : @root.push(v)
    end

    # load index from the index file and construct the search tree recursively
    # params:
    # - root: current tree node
    # - file: index file with tree data
    def deserialize(root, file)
      return nil if file.eof?

      line = file.readline
      # create empty tree node if line has -1 (leaf)
      return BinaryTreeSearch::EmptyNode.new if !line.index('-1').nil?

      # parse line format into value hash
      idx = line.strip.split("|")
      key = idx.shift
      v = idx.map{|i| i.split(":")}.to_h

      # create node and go down the search tree
      root = BinaryTreeSearch::Node.new({key.to_s => v})
      root.left = deserialize(root.left, file)
      root.right = deserialize(root.right, file)
      return root
    end

    # save index file
    # params:
    # - filename: index file name
    def save(filename=nil)
      filename = @index_filename if filename.nil?
      File::open(filename, "w") do |f|
        # save file name dictionary into first index file line
        f.puts @dict.join("|")
        # save search tree
        @root.serialize(f)
      end
    end

    # load index file
    # params:
    # - filename: index file name
    def load(filename=nil)
      filename = @index_filename if filename.nil?
      @root = nil
      if File.file?(filename)
        File::open(filename, "r") do |f|
          return nil if f.eof?
          # load file name dictionary from the first line of index file
          @dict = f.readline.strip.split("|")
          # load search tree
          @root = deserialize(@root, f)
        end
      end
      return @root
    end

    # create index entries for text file (indexing)
    # params:
    # - filename: text file name to create index for
    def add_to_index(filename)
      filename_idx = nil   # index of the text file in the dictionary
      file_line_num = -1   # number of line in the filr

      File::open(filename, "r") do |file|
        file.readlines.each do |line|
          file_line_num += 1
          # find text file index in dictionary to use it in indexing
          @dict << filename if !@dict.include?(filename)
          filename_idx = @dict.find_index(filename)

          # find words in the line (and remove occasional empty entries)
          words = line.split(/[^[[:word:]]]+/) - [""]
          words.each do |word|
            if !@root.nil?
              # add number of the line where word being indexed is found
              @root.push({word.to_s => {"#{filename_idx.to_s}" => "#{file_line_num}"}})
            else
              # create root node
              @root = BinaryTreeSearch::Node.new({word.to_s => {"#{filename_idx.to_s}" => "#{file_line_num}"}})
            end
          end
        end
      end
      @root
    end

    # get index entries for word
    # params:
    # - word: word to find index data for
    def entries_for(word)
      # use Node method
      entries = @root.entries_for(word)
      # substitute file name numeric index from index file entry
      # with file name from the dictionary
      entries.keys.each do |k|
        entries[@dict[k.to_i]] = entries.delete(k.to_s)
      end
      entries
    end

    # find out if word is in the search tree
    # params:
    # - word: word to look for in the search tree
    def include?(word)
      @root.include?(word)
    end

    # service method, run indexing for file
    # params:
    # - file_to_index: file to run index for
    def self.run_index(file_to_index=nil)
      file_to_index ||= ARGV[0]
      return if file_to_index.nil?

      t = IndexTree.new
      t.load
      t.add_to_index(file_to_index)
      t.save
    end

  end
end

# Tree.run_index
require './empty_node.rb'

module BinaryTreeSearch

  # Represents the node of the binary search tree
  class Node
    attr_reader :value
    # links to the left and right nodes
    attr_accessor :left, :right

    def initialize(v)
      @value = v
      @left  = EmptyNode.new
      @right = EmptyNode.new
    end

    # add value to the tree node for already existing index key (word)
    # (if the same word is found on another line or in another file)
    def push_to_node(v)
      # value e.g. {"word" => {"0" => "1,27,189"}}
      key = value.keys.first      # key and value from the index entry node
      val = value[key]            # key: "word", val: {"0" => "1,27,189"}

      v_val = v[key]              # new value to add/update for the same key
                                  # e.g. v_val:{"1" => "45"}

      add_key = v_val.keys.first  # file name dictionary index key
                                  # add_key: "1"
      add_val = v_val[add_key]    # word index info for file
                                  # add_val: "45"

      # insert new index information or update existing
      # from: {"word" => {"0" => "1,27,189"}}
      # into: {"word" => {"0" => "1,27,189", "1" => "45"}}
      val[add_key] = val[add_key].nil? ? "#{add_val}"
                                       : "#{val[add_key]},#{add_val}"
      return false
    end

    # push value into the search tree recursively
    def push(v)
      case value.keys[0] <=> v.keys[0]
      when  1 then push_left(v)
      when -1 then push_right(v)
      when  0 then push_to_node(v) # node value insert/update
      end
    end

    # find out if there a value in the search tree
    def include?(v)
      case value.keys[0] <=> v
      when  1 then left.include?(v)
      when -1 then right.include?(v)
      when  0 then true # the current node is equal to the value
      end
    end

    # get tree entries for word value
    def entries_for(word)
      case value.keys.first <=> word
      when  1 then left.entries_for(word)
      when -1 then right.entries_for(word)
      when  0 then value[value.keys.first]
      end
    end

    def inspect
      "{#{value}: LE: #{left.inspect} | RI: #{right.inspect}}"
    end

    def to_a
      left.to_a + [value] + right.to_a
    end

    # save node value into the index file
    def serialize(file)
      key = value.keys.first
      val = value[key].inject(''){|ac, (k, v)| ac += "|#{k}:#{v}"}
      file.puts "#{key}#{val}"

      left.serialize(file) if left
      right.serialize(file) if right
    end

    private

      # push value into the left branch
      # create node if in the leaf node
      def push_left(v)
        self.left.instance_of?(EmptyNode) ? self.left = Node.new(v)
                                          : self.left.push(v)
        return self.left
      end

      # push value into the right branch
      # create node if in the leaf node
      def push_right(v)
        self.right.instance_of?(EmptyNode) ? self.right = Node.new(v)
                                           : self.right.push(v)
        return self.right
      end
  end

end
#!/usr/bin/env ruby

require "./index_tree.rb"
include BinaryTreeSearch

def usage
  puts "Usage: ./index.rb <text_file_to_be_indexed>"
  exit 0
end

# check if file is binary by using system command 'file'
def is_binary?(file)
  %x(file #{file}) !~ /text/
end

usage if ARGV.empty?

if !File.file?(ARGV[0])
  puts "File #{ARGV[0]} is not found"
  exit 0
end

if is_binary?(ARGV[0])
  puts "File #{ARGV[0]} is a binary file, cannot index"
  exit 0
end

IndexTree.run_index ARGV[0]
## Index search application ##

Test application uses binary search tree text index to perform text file indexing and word search.

Index file is a serialized binary search tree. Index file format is text, first line is a dictionary for indexed file's names.

Search is case-sensitive intentionally, 'The' and 'the' are the separate words.

Binary files are not supported.


### Indexing ###

To create index for the text file, run `index.rb`

```
$ ./index.rb <file_name>
```

Index file is created or updated, filename by default is `id.x`


### Search ###

To search words in index, run `search.rb`

``` 
$ ./search.rb <word1> <word2> <...>
```

or

``` 
$ ./search.rb <word1> + <word2> + <...>
```

to search by AND-condition

or a combination of parameters

``` bash
$ ./search.rb <word1> <word2> + <word3> <word4> <...>
```


### Examples ###

Find files that have the words Alice, in, Wonderland in them

```sh
$ ./search.rb Alice in Wonderland
Searching for 'Alice':
        Found in:
                alice.txt
Searching for 'in':
        Found in:
                alice.txt
                mobydick.txt
                50shades.txt
Searching for 'Wonderland':
        Found in:
                alice.txt
```


Find files that have words Alice, in, Wonderland in a single file (in any order):

```sh
$ ./search.rb Alice + in + Wonderland
Searching for 'Alice + in + Wonderland':
        Found in:
                alice.txt
```

### Platform ###

ruby 2.3.0p0 (2015-12-25 revision 53290) [x86_64-linux]
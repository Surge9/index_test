#!/usr/bin/env ruby

require './index_tree.rb'
include BinaryTreeSearch

  OP_AND = "+"  # symbol for AND-conditional search

  def usage
    puts "Usage: search.rb <space-separated words> for search for words in files via index"
    puts " Also: search.rb <string1 + string2 [+ string3...]> for AND search via index"
  end

  # parse search string for conditional AND-search
  # conditions are grouped into the array
  # example (OP_AND is '+'):
  #   search_string:  "1 2 + 3 + 4 5 6 7 + 8 9"
  #   return:        ["1", ["+", "2", "3", "4"], "5", "6", ["+", "7", "8"], "9"]
  # pararms:
  # - search_string: string contains input for word search
  def prep_conditional(search_string)
    tokens = search_string.split
    t = [], i = 0

    while true do
      break if tokens[i].nil? # break if out of array bounds

      # find all AND-search groups
      if OP_AND == tokens[i]
        op1 = tokens[i-1]
        op2 = tokens[i+1]
        if op1 && op2
          unless (op1.is_a? Array)
            # case: op1 = "<>", OP_AND, op2
            t << OP_AND << op1 << op2
            3.times {tokens.delete_at(i-1)}
            tokens.insert(i-1, t)
          else
            # case: op1 = [OP_AND, <>, <>], OP_AND, op2
            t = op1 << op2
            2.times {tokens.delete_at(i)}
          end
          i -= 1
        end
      end
      t = []
      i += 1
    end
    return tokens
  end

  # search recursively for words in the index tree
  # params:
  # - search_element: words to search for
  # - index_tree: index search tree
  def search_for(search_element, index_tree)
    if search_element.is_a?(Array)
      # search_element is an AND-array: [OP_AND, v1, v2, ...]
      found = []
      search_element -= [OP_AND]  # remove AND-string to leave only words
      search_element.each do |s|
        # call search for the next word from the list
        res = search_for(s, index_tree) || []
        # intersect result arrays
        found.empty? ? found = res : found &= res
      end
      return found
    else
      # search_element is a word
      found = index_tree.entries_for(search_element)
      if !found.empty?
        # The number of file entries was saved into the index
        # as a list of line numbers where word was found.
        # Result is sorted by the number of entries
        # so files with the most occurences will be listed on top
        # {a:"1", b:"1,2,3", z:"4,7"} -> {b:"1,2,3", z:"4,7", a:"1"}
        return Hash[found.sort_by{ |k,v| -v.split(',').length }].keys
      end
    end
  end

  # get list of words to search
  words_to_search = ARGV.to_a
  if words_to_search.nil? || words_to_search.empty?
    usage
    puts "No words to search, exiting"
    exit 0
  end

  # prepare conditional search
  words_to_search = prep_conditional(ARGV.join(" "))

  # create and load index tree
  index_tree = IndexTree.new
  index_tree.load

  # start search
  words_to_search.each do |w|
    if w.is_a?(Array)
      puts "Searching for '#{w.select{|s| s != '+'}.join(" + ")}':"
    else
      puts "Searching for '#{w}':"
    end

    result = search_for(w, index_tree)
    if result.nil? || result.empty?
      puts "\tNot found"
    else
      puts "\tFound in:"
      result.each {|r| puts "\t\t#{r}"}
    end
  end